package jc_tarby.ieea.univ_lille_1.fr.tp_startactivity_ratingbar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class Demarrage extends AppCompatActivity {

    // pour solution 2/3/4
	 private static final int MONCODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demarrage);
    }

    public void lance_activite(View v) {
        Intent intent = new Intent(getApplicationContext(), Suite.class);

        //pour solution 4 (passage de paramètre)
        float vote_initial = 2;
        intent.putExtra("vote_initial", vote_initial);

        // pour solution 2/3/4
        startActivityForResult(intent, MONCODE);

//        // pour solution 1 uniquement
//        startActivity(intent);
    }

    // pour solution 2/3/4
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// super.onActivityResult(requestCode, resultCode, data);
		float vote;

		switch (requestCode) {
		case MONCODE:
			switch (resultCode) {
			case RESULT_OK:
				// pour solution 2, 3 et 4
				Toast.makeText(getApplicationContext(), "Merci d'avoir voté",
						Toast.LENGTH_SHORT).show();
				// pour solution 3 et 4
				vote = data.getFloatExtra("vote", -100);
				Toast.makeText(getApplicationContext(),
						"vous avez voté " + vote, Toast.LENGTH_SHORT).show();
				break;
			case RESULT_CANCELED:
				// pour solution 2, 3 et 4
				Toast.makeText(getApplicationContext(),
						"Pourquoi ne pas avoir voté ?", Toast.LENGTH_SHORT)
						.show();
				// pour solution 3 et 4
				if (data != null) {
					vote = data.getFloatExtra("vote", -100);
					Toast.makeText(getApplicationContext(),
							"vote pas ok, cela vaut " + vote,
							Toast.LENGTH_SHORT).show();
				} else
					Toast.makeText(getApplicationContext(),
							"vous n'avez pas voté", Toast.LENGTH_SHORT).show();
				break;
			default:
				break;
			}
			break;

		default:
			break;
		}
	}
}

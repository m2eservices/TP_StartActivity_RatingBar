package jc_tarby.ieea.univ_lille_1.fr.tp_startactivity_ratingbar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RatingBar;
import android.widget.Toast;

public class Suite extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.suite);

        // c'est juste pour montrer que le onPause est appelé après que
        // l'activité soit passée en arrière-plan (et après une rotation
        // d'écran)
        Toast.makeText(getApplicationContext(), "on Create", Toast.LENGTH_SHORT)
                .show();

		/* (ne pas enlever les commentaires)
         * plutôt que d'initialiser les composants IHM ici, on le fait dans le
		 * onStart (meilleur)
		 *
		 *

        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar1);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Intent intent = getIntent();
                intent.putExtra("vote", rating);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        if (getIntent().getExtras() != null) {
            float vote =
                    getIntent().getExtras().getFloat("vote_initial");
            ratingBar.setRating(vote);
        }
        */
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar1);

        // solution 4

        // astuce :
        // si on met ce code APRES le listener, le fait de faire un setRating
        // équivaut à faire un clic sur la ratingBar donc cela déclenche
        // automatiquement le listener et donc on n'a pas le temps de voter en
        // cliquant sur la ratingBar !
        // SAUF si on gère le "fromUser" !
       if (getIntent().getExtras() != null) {
            float vote = getIntent().getExtras().getFloat("vote_initial");
            ratingBar.setRating(vote);
        }

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                // solution 2, 3, 4
                 Intent intent = getIntent();

                // solution 3
                intent.putExtra("vote", rating);

                // solution 2, 3
                setResult(RESULT_OK, intent);

                // solution 1, 2, 3, 4
                if (fromUser)
                    // solution 1
                    finish();
            }
        });

        // solution 4
		if (getIntent().getExtras() != null) {
			float vote = getIntent().getExtras().getFloat("vote_initial");
			ratingBar.setRating(vote);
		}
    }

    // Laisser cette méthode commentée pour le premier test
    // puis la décommenter pour montrer l'intérêt du "fromUser" pour la ratingBar
   /* @Override
    protected void onResume() {
        // TODO Auto-generated method stub

        // c'est juste pour montrer que le onResume est appelé après le
        // onCreate (y compris après une rotation de l'écran !)
        super.onResume();
        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar1);

        // chaque fois on augmente de 1
        // ATTENTION : le setRatings équivaut à faire un clic sur la ratingBar donc cela déclenche
        // automatiquement le listener et donc on n'a pas le temps de voter en cliquant sur la ratingBar !
        // SAUF si on gère le "fromUser" !
        ratingBar.setRating(ratingBar.getRating() + 1);
    }*/

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub

        // c'est juste pour montrer que le onPause est appelé après que
        // l'activité soit passée en arrière-plan (et après une rotation
        // d'écran)
        super.onPause();
        Toast.makeText(getApplicationContext(), "on Pause", Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub

        // c'est juste pour montrer que le onPause est appelé après que
        // l'activité soit passée en arrière-plan (et après une rotation
        // d'écran)
        super.onStop();
        Toast.makeText(getApplicationContext(), "on Stop", Toast.LENGTH_SHORT)
                .show();
    }

    // solution 2, 3, 4
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        //super.onBackPressed();
        Intent intent = getIntent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

}
